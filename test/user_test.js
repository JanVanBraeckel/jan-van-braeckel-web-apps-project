require('./init_test.js');
var expect = require('chai').expect;
var app = require('../src/server/app');
var request = require('supertest');
var agent = request.agent(app);

describe('POST /login', function () {
    it('should respond with 200 in case of valid post, this is just a quick test (be sure to make user "jan" with password "123456")', function (done) {
        agent
            .post('/login')
            .send({
                'username': 'jan',
                'password': '123456'
            })
            .expect(200)
            .end(function (err, res) {
                if (err) { return done(err); }

                var fetchedData = JSON.parse(res.text);
                expect(fetchedData).to.not.empty;

                if (fetchedData) {
                    expect(fetchedData).to.have.all.keys('token');
                    done();
                }
            });
    });
});
