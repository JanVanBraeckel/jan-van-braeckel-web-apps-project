require('./init_test.js');
var expect = require('chai').expect;
var app = require('../src/server/app');
var request = require('supertest');
var agent = request.agent(app);

describe('GET /posts', function () {
    it('should respond with 200 in case of valid request', function (done) {
        agent
            .get('/posts/')
            .send()
            .expect(200)
            .end(function (err, res) {
                if (err) { return done(err); }

                var fetchedData = JSON.parse(res.text);
                //console.Log('fecthedData:', fetchedData);
                expect(fetchedData).to.be.an('array');
                expect(fetchedData).to.not.empty;

                var post = fetchedData[0];

                console.log(post);

                if (post) {
                    expect(post).to.have.all.keys('__v', '_id', 'comments', 'upvotes', 'author', 'title', 'link');
                    expect(post.comments).to.be.an('array');
                    expect(post.upvotes).to.be.a('number');
                    done();
                }
            });
    });
});
