# README #

[Website URL is hier te vinden (http://flappernews-janvanbraeckel.rhcloud.com/)](http://flappernews-janvanbraeckel.rhcloud.com/)

### Wat is deze repository? ###

Deze repository is een uitwerking van de oefening FlapperNews die we tijdens Web Apps gezien hebben bij Joeri Van Steen (Hogeschool Gent).

### Hoe run ik? ###

* npm install && bower install in de root (mogelijks admin/su nodig)
* mongo starten
* gulp serve-dev voor dev omgeving
* gulp serve-build voor build omgeving
* gulp runTests voor tests te runnen (zorg voor gebruiker jan met wachtwoord 123456, slechte manier van testen maar ik wou het eens proberen)

### Wat is er veranderd buiten de les? ###

* Downvotes op comments en posts
* Angular Material gebruikt
* Refactoring (niet zoals in de laatste les, jammer genoeg geen tijd meer voor gehad)
* Vooral bezig gehouden met gulp, typ gulp, gulp default of gulp help voor task listing