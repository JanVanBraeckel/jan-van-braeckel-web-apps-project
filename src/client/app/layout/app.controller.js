angular
	.module('flapperNews')
	.controller('AppCtrl', AppCtrl);

function AppCtrl($scope, $mdSidenav) {
	$scope.toggleSidenav = function (menuId) {
		$mdSidenav(menuId).toggle();
	};
}