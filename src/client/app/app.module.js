angular
    .module('flapperNews', [
        'ui.router',
        'ngMaterial',
        'ngMdIcons',
        'ngAnimate'
    ]);
