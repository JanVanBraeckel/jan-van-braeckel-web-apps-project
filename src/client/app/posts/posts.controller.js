angular
	.module('flapperNews')
	.controller('PostsCtrl', PostsCtrl);

function PostsCtrl($scope, postFactory, post, auth) {
	$scope.post = post;
    $scope.isLoggedIn = auth.isLoggedIn;

    $scope.addComment = function () {
		if (!$scope.body || $scope.body === '') {
			return;
		}

		postFactory.addComment(post._id, {
			body: $scope.body,
			author: 'user',
		}).success(function (comment) {
			$scope.post.comments.push(comment);
		});

		$scope.body = '';
    };

    $scope.incrementUpvotes = function (comment) {
		postFactory.upvoteComment(post, comment);
    };

    $scope.decreaseUpvotes = function (comment) {
		postFactory.downvoteComment(post, comment);
    };
}
