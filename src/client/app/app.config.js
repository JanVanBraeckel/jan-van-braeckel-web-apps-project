angular
    .module('flapperNews')
    .config(config);

function config($stateProvider, $urlRouterProvider) {
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'app/home/home.html',
        controller: 'HomeCtrl',
        resolve: {
            postPromise: ['postFactory', function (postFactory) {
                return postFactory.getAll();
            }]
        }
    }).state('posts', {
        url: '/posts/{id}',
        templateUrl: 'app/posts/posts.html',
        controller: 'PostsCtrl',
        resolve: {
            post: ['$stateParams', 'postFactory', function ($stateParams, postFactory) {
                return postFactory.get($stateParams.id);
            }]
        }
    }).state('login', {
        url: '/login',
        templateUrl: 'app/authentication/login.html',
        controller: 'AuthCtrl',
        onEnter: ['$state', 'auth', function ($state, auth) {
            if (auth.isLoggedIn()) {
                $state.go('home');
            }
        }]
    }).state('register', {
        url: '/register',
        templateUrl: 'app/authentication/register.html',
        controller: 'AuthCtrl',
        onEnter: ['$state', 'auth', function ($state, auth) {
            if (auth.isLoggedIn()) {
                $state.go('home');
            }
        }]
    });

    $urlRouterProvider.otherwise('home');
}