angular
	.module('flapperNews')
	.controller('HomeCtrl', HomeCtrl);
	
function HomeCtrl($scope, postFactory, auth){
	$scope.posts = postFactory.posts;
    $scope.isLoggedIn = auth.isLoggedIn;

    $scope.addPost = function () {
      if (!$scope.title || $scope.title === '') {
        return;
      }
      postFactory.create({
        title: $scope.title,
        link: $scope.link,
      });
      $scope.title = '';
      $scope.link = '';
    };

    $scope.incrementUpvotes = function (post) {
      postFactory.upvote(post);
    };

    $scope.decreaseUpvotes = function (post) {
      postFactory.downvote(post);
    };
}